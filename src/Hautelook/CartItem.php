<?php
/**
 * Created by Sofiane Oumaouche.
 * User: Sofiane
 * Date: 12/7/15
 * Time: 11:06 PM
 */

namespace Hautelook;


class CartItem {

    private $name;

    private $price;

    private $weight;

    private $quantity;

    /**
     * @param $name
     * @param $price
     * @param int $weight
     */
    public function __construct($name, $price, $weight = 0)
    {
        $this->name = $name;
        $this->price = $price;
        $this->weight = $weight;
    }

    /**
     * @param $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return mixed
     */
    public function price()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function name()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function weight()
    {
        return $this->weight;
    }

    /**
     * @return mixed
     */
    public function quantity()
    {
        return $this->quantity;
    }

} 