<?php
namespace Hautelook;

use Doctrine\Common\Collections\ArrayCollection;

class Cart
{
    const  HEAVY_WEIGHT = 10;
    const  HEAVY_WEIGHT_SHIPPING_COST = 20;
    const  SHIPPING_FLAT_RATE = 5;

    private $items;
    private $couponPercentage = 0;

    public function __construct(){
        $this->items = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function subtotal()
    {
        $subtotal = 0;
        $this->items->map(function($item) use (&$subtotal)
        {
            $subtotal += ($item->price() * $item->quantity());
        });

        $subtotal = $subtotal - $subtotal*$this->couponPercentage;

        return $subtotal;
    }

    /**
     * @return int
     */
    public function total()
    {
        return $this->subtotal() + $this->shipping();
    }

    /**
     * @param CartItem $item
     */
    public function addItem(CartItem $item)
    {
        if($this->items->containsKey($item->name()))
        {
            $cartItem = $this->items->get($item->name());
            $cartItem->setQuantity($cartItem->quantity() + $item->quantity());

        }else{
            $this->items->set($item->name(), $item);
        }
    }

    /**
     * @param $product_name
     * @return CartItem
     */
    public function getItem($product_name)
    {
        $item = $this->items->get($product_name);

        return $item;
    }

    /**
     * @param $couponDiscount
     */
    public function applyCouponDiscount($couponDiscount)
    {
        $this->couponPercentage = $couponDiscount/100;
    }

    /**
     * @return int
     */
    public function shipping()
    {
        $shipping = 0;

        if($this->subtotal() < 100)
        {
            $shipping = self::SHIPPING_FLAT_RATE;
        }

        $heavyItems = $this->items->filter(function($item){
            return ($item->weight() >= self::HEAVY_WEIGHT);
        });

        $heavyItems->map(function($item) use (&$shipping){
            $shipping += $item->quantity() * self::HEAVY_WEIGHT_SHIPPING_COST;
        });

        return $shipping;
    }

}
